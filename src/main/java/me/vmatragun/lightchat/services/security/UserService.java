package me.vmatragun.lightchat.services.security;

import lombok.extern.slf4j.Slf4j;
import me.vmatragun.lightchat.domain.ApplicationUser;
import me.vmatragun.lightchat.repository.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@Transactional
public class UserService {

  @Autowired
  private ApplicationUserRepository applicationUserRepository;

  public Page<ApplicationUser> findAll(Pageable pageable) {
    return applicationUserRepository.findAll(pageable);
  }

  public ApplicationUser findOne(String id) {
    return applicationUserRepository.findOne(id);
  }


  public ApplicationUser save(ApplicationUser user) {
    return applicationUserRepository.save(user);
  }
}
