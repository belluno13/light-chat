package me.vmatragun.lightchat.services;

import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by vmatragun on 6/16/17.
 */
@Component
public class EncodingService {

  public static final String UTF_8 = "UTF-8";

  public String encode(String value) {
    try {
      return URLEncoder.encode(value, UTF_8).replace(".", "%2E");
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  public String decode(String value) {
    try {
      return URLDecoder.decode(value, UTF_8);
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }
}
