package me.vmatragun.lightchat.repository;

import me.vmatragun.lightchat.domain.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ApplicationUserRepository extends PagingAndSortingRepository<ApplicationUser, String> {
  ApplicationUser findByUsername(String username);
}