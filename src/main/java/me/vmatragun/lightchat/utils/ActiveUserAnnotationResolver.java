package me.vmatragun.lightchat.utils;

import me.vmatragun.lightchat.domain.ApplicationUser;
import org.springframework.core.MethodParameter;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.security.Principal;

public class ActiveUserAnnotationResolver implements HandlerMethodArgumentResolver {
  @Override
  public boolean supportsParameter(MethodParameter methodParameter) {
    return methodParameter.getParameterAnnotation(ActiveUser.class) != null
            && methodParameter.getParameterType().equals(ApplicationUser.class);
  }

  @Override
  public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
    if (this.supportsParameter(methodParameter)) {
      Principal principal = nativeWebRequest.getUserPrincipal();
      if (principal == null) {
        return null;
      }

      ApplicationUser user = (ApplicationUser) ((Authentication) principal).getPrincipal();

      return user;
    } else {
      return WebArgumentResolver.UNRESOLVED;
    }
  }
}
