package me.vmatragun.lightchat.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vmatragun on 6/9/17.
 */

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

  @RequestMapping(method =  RequestMethod.GET)
  public HttpEntity<TestResource> getOne(){
    log.error("test");
    return new ResponseEntity<>(new TestResource("Hello world"), HttpStatus.OK);
  }

}
