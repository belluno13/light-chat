package me.vmatragun.lightchat.controller.user;

import me.vmatragun.lightchat.controller.UserController;
import me.vmatragun.lightchat.domain.ApplicationUser;
import me.vmatragun.lightchat.services.EncodingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by vmatragun on 6/16/17.
 */
@Component
public class UserResourceAssembler implements ResourceAssembler<ApplicationUser, UserResource> {

  @Autowired
  private EncodingService encodingService;

  @Override
  public UserResource toResource(ApplicationUser user) {
    UserResource userResource = new UserResource(user);
    userResource.add(linkTo(methodOn(UserController.class).find(encodingService.encode(user.getId()))).withSelfRel());
    return userResource;
  }
}
