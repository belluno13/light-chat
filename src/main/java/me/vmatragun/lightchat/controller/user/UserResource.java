package me.vmatragun.lightchat.controller.user;

import me.vmatragun.lightchat.domain.ApplicationUser;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by vmatragun on 6/16/17.
 */
public class UserResource extends ResourceSupport {

  private ApplicationUser user;

  public UserResource(ApplicationUser user) {
    this.user = user;
  }

  public String getUserId() {
    return this.user.getId();
  }

  public String getEmail() {
    return this.user.getEmail();
  }

  public String getDisplayName() {
    return this.user.getDisplayName();
  }

}
