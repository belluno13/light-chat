package me.vmatragun.lightchat.controller;

import lombok.extern.slf4j.Slf4j;
import me.vmatragun.lightchat.controller.user.UserResource;
import me.vmatragun.lightchat.domain.ApplicationUser;
import me.vmatragun.lightchat.repository.ApplicationUserRepository;
import me.vmatragun.lightchat.services.EncodingService;
import me.vmatragun.lightchat.services.security.UserService;
import me.vmatragun.lightchat.controller.user.UserResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by vmatragun on 6/9/17.
 */
@Slf4j
@RestController
@RequestMapping(path = "/users")
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  private EncodingService encodingService;

  @Autowired
  private UserResourceAssembler userResourceAssembler;

  private ApplicationUserRepository applicationUserRepository;
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  public UserController(ApplicationUserRepository applicationUserRepository,
                        BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.applicationUserRepository = applicationUserRepository;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @PostMapping("/sign-up")
  public void signUp(@RequestBody ApplicationUser user) throws NoSuchAlgorithmException {
    String id = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(user.getEmail().getBytes()));
    String password = bCryptPasswordEncoder.encode(user.getPassword());
    user.setId(id);
    user.setPassword(password);
    applicationUserRepository.save(user);
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  public HttpEntity<UserResource> find(@PathVariable String id) {
    ApplicationUser user = userService.findOne(encodingService.decode(id));
    return new ResponseEntity<>(userResourceAssembler.toResource(user), HttpStatus.OK);
  }


  @RequestMapping(method = RequestMethod.POST)
  public HttpEntity<UserResource> create(@Validated @RequestBody ApplicationUser user) {
    ApplicationUser savedUser = userService.save(user);
    return new ResponseEntity<>(userResourceAssembler.toResource(savedUser), HttpStatus.CREATED);
  }

}
