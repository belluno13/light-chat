package me.vmatragun.lightchat.controller;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by vmatragun on 6/9/17.
 */

public class TestResource extends ResourceSupport {

  private String testString;

  public TestResource(String testString) {
    this.testString = testString;
  }

  public String getTestString() {
    return testString;
  }
}
