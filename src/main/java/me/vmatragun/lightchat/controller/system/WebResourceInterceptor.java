package me.vmatragun.lightchat.controller.system;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

@Slf4j
@RestController
@RequestMapping(path = "/")
public class WebResourceInterceptor {

  public static final String SRC_MAIN_WEBAPP = "./src/main/webapp";
  public static final String SRC_MAIN_WEBAPP_WEB_INF = SRC_MAIN_WEBAPP + "/node_modules";

  @RequestMapping(method = RequestMethod.GET)
  public FileSystemResource getRoot() {
    File file = new File(SRC_MAIN_WEBAPP + "/index.html");
    return new FileSystemResource(file);
  }

  @RequestMapping(path = "resources/**", method = RequestMethod.GET)
  public FileSystemResource getFile(HttpServletRequest request) {
    File file = new File(SRC_MAIN_WEBAPP_WEB_INF + request.getServletPath().replace("/resources",""));

    return new FileSystemResource(file);
  }

  @RequestMapping(path = "favicon.ico", method = RequestMethod.GET)
  public FileSystemResource geticon(HttpServletRequest request) {
    File file = new File(SRC_MAIN_WEBAPP_WEB_INF + request.getServletPath());

    return new FileSystemResource(file);
  }

}
