package me.vmatragun.lightchat.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.vmatragun.lightchat.domain.ApplicationUser;
import me.vmatragun.lightchat.repository.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import static me.vmatragun.lightchat.security.SecurityConstants.EXPIRATION_TIME;
import static me.vmatragun.lightchat.security.SecurityConstants.SECRET;
import static me.vmatragun.lightchat.security.SecurityConstants.HEADER_STRING;
import static me.vmatragun.lightchat.security.SecurityConstants.TOKEN_PREFIX;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
  private AuthenticationManager authenticationManager;
  private ObjectMapper mapper;
  private ApplicationUserRepository applicationUserRepository;


  public JWTAuthenticationFilter(AuthenticationManager authenticationManager, ObjectMapper mapper, ApplicationUserRepository applicationUserRepository) {
    this.authenticationManager = authenticationManager;
    this.mapper = mapper;
    this.applicationUserRepository = applicationUserRepository;
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest req,
                                              HttpServletResponse res) throws AuthenticationException {
    try {
      ApplicationUser creds = new ObjectMapper()
              .readValue(req.getInputStream(), ApplicationUser.class);

      return authenticationManager.authenticate(
              new UsernamePasswordAuthenticationToken(
                      creds.getUsername(),
                      creds.getPassword())
      );
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest req,
                                          HttpServletResponse res,
                                          FilterChain chain,
                                          Authentication auth) throws IOException, ServletException {

    ApplicationUser byUsername = applicationUserRepository.findByUsername(auth.getName());
    Map<String, String> appData = new HashMap();
    appData.put("user", byUsername.getUsername());
    appData.put("email", byUsername.getEmail());
    String token = Jwts.builder().claim("id", byUsername.getId()).claim("app-meta", appData)
            .setSubject(((User) auth.getPrincipal()).getUsername())
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
            .compact();
    res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);

    HashMap<String, String> response = new HashMap();
    response.put("token", token);
    mapper.writeValue(res.getWriter(), response);
  }
}