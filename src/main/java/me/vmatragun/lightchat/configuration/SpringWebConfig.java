package me.vmatragun.lightchat.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Slf4j
@EnableWebMvc //mvc:annotation-driven
@Configuration
@ComponentScan({ "me.vmatragun.lightchat" })
public class SpringWebConfig extends WebMvcConfigurerAdapter {

}