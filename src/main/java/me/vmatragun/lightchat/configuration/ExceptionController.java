package me.vmatragun.lightchat.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class ExceptionController {

  @ExceptionHandler(Exception.class)
  public ModelAndView handleError(HttpServletRequest request, Exception e) {
    log.error("Exception", e);
    return new ModelAndView("error");
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  public ModelAndView handleError404(HttpServletRequest request, Exception e) {
    log.error("Exception", e);
    return new ModelAndView("404");
  }
}