CREATE TABLE users (
  id                  VARCHAR(44) PRIMARY KEY,
  display_name        VARCHAR(13),
  username            VARCHAR(13),
  email               VARCHAR(20),
  password            VARCHAR(60) NOT NULL,
  enabled             BOOLEAN DEFAULT TRUE
);

insert into users (id, display_name, email, password) values ('1','user1','email1@mail.com','testpass');
insert into users (id, display_name, email, password) values ('2','user2','email2@mail.com','testpass');
insert into users (id, display_name, email, password) values ('3','user3','email3@mail.com','testpass');

