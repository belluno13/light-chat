package features.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class DummyStepdefs {

  @When("^I do something$")
  public void iDoSomething() throws Throwable {
    // actually I do nothing
  }

  @Then("^it does not fail$")
  public void itDoesNotFail() throws Throwable {
    // just a dummy assertion
    Assert.assertTrue(true);
  }
}
