package features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@Slf4j
@RunWith(Cucumber.class)
@CucumberOptions( plugin = {"pretty", "html:target/cucumber"}, tags = "~@ignore")
public class RunCukes {

  @BeforeClass
  public static void setupEnvironment() throws Exception {
      log.info("setup class");
  }

}
